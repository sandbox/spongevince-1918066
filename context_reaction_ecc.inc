<?php

/**
 * @file
 * Context Evolutive Cache Controlcontext reaction file
 */

class ContextReactionEcc extends context_reaction {

  /**
   * Context reaction form.
   *
   * @param context $context
   *   Context object
   */
  public function options_form($context) {
    $values = $this->fetch_from_context($context);

    drupal_add_js(drupal_get_path('module', 'context_ecc') . '/js/context_ecc.js');

    $items = array(
      '#title' => t('Evolutive Cache Control'),
      '#tree' => 'tree',
    );

    $items['rewrite'] = array(
      '#type' => 'checkbox',
      '#title' => 'Override another cache control settings',
      '#default_value' => (isset($values['rewrite'])) ? $values['rewrite'] : FALSE,
    );

    $num_checkboxes = !empty($values['howmanylevels_select']) ? $values['howmanylevels_select'] : 1;

    $options = array();
    for ($i = 1; $i <= 10; $i++) {
      $options[$i] = $i;
    }

    $items['howmanylevels_select'] = array(
      '#title' => t('How many Cache Control Levels do you want?'),
      '#description' => t('Multiple level of cache control is only used with node entity conditions'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $num_checkboxes,
      '#ajax' => array(
        'callback' => '_context_ecc_context_reaction_form_howmanylevels_ajax_callback',
        'wrapper' => 'levels',
        'effect' => 'fade',
      ),
    );

    $items['ecc_levels'] = array(
      '#title' => t("Cache Control Levels"),
      '#prefix' => '<div id="levels">',
      '#suffix' => '</div>',
      '#type' => 'fieldset',
    );

    for ($i = 1; $i <= $num_checkboxes; $i++) {
      $items['ecc_levels']["level$i"] = array(
        '#type' => 'fieldset',
        '#title' => "Level $i",
      );

      $infos_markup_cache = ' seconds, Set cache to ';
      if ($num_checkboxes == 1) {
        $infos_markup_1 = '';
        $infos_markup_2 = '';
        $infos_markup_cache = 'Set cache to ';
        $mode = 'one_value';
      }
      elseif ($i == 1) {
        $infos_markup_1 = '';
        $infos_markup_2 = 'If content has been modified in the last ';
        $mode = 'lower_value';
      }
      elseif ($i == $num_checkboxes) {
        $infos_markup_1 = 'If content modifications are older than ';
        $infos_markup_2 = '';
        $mode = 'higher_value';
      }
      else {
        $infos_markup_1 = ' If content has been modified betwween ';
        $infos_markup_2 = ' seconds and ';
        $mode = 'between_value';
      }

      $items['ecc_levels']["level$i"]["mode"] = array(
        '#type' => 'hidden',
        '#value' => check_plain($mode),
      );

      $items['ecc_levels']["level$i"]["second_since_last_change_infos_1"] = array(
        '#type' => 'markup',
        '#markup' => check_plain($infos_markup_1),
        '#prefix' => '<div class="context-ecc-infos">',
        '#suffix' => '</div>',
      );

      $precedent = $i - 1;
      if ($i > 1) {
        $items['ecc_levels']["level$i"]["second_since_last_change_first"] = array(
          '#type' => 'textfield',
          '#maxlength' => '6',
          '#size' => '6',
          '#attributes' => array(
            'readonly' => 'readonly',
          ),
          '#default_value' => (isset($values['ecc_levels']["level$i"]["second_since_last_change_first"])) ? $values['ecc_levels']["level$i"]["second_since_last_change_first"] : '',
          '#prefix' => '<div class="second-since-last-change">',
          '#suffix' => '</div>',
        );
      }

      $items['ecc_levels']["level$i"]["second_since_last_change_infos_2"] = array(
        '#type' => 'markup',
        '#markup' => check_plain($infos_markup_2),
        '#prefix' => '<div class="context-ecc-infos">',
        '#suffix' => '</div>',
      );

      if ($num_checkboxes >= 1 && $num_checkboxes != $i) {
        $items['ecc_levels']["level$i"]["second_since_last_change_second"] = array(
          '#type' => 'textfield',
          '#maxlength' => '6',
          '#size' => '6',
          '#required' => TRUE,
          '#default_value' => (isset($values['ecc_levels']["level$i"]["second_since_last_change_second"])) ? $values['ecc_levels']["level$i"]["second_since_last_change_second"] : '',
          '#prefix' => '<div class="second-since-last-change">',
          '#suffix' => '</div>',
        );
      }

      $items['ecc_levels']["level$i"]["second_since_last_change_infos_3"] = array(
        '#type' => 'markup',
        '#markup' => $infos_markup_cache,
        '#prefix' => '<div class="context-ecc-infos">',
        '#suffix' => '</div>',
      );
      $items['ecc_levels']["level$i"]["cache_value"] = array(
        '#type' => 'textfield',
        '#maxlength' => '6',
        '#size' => '6',
        '#required' => TRUE,
        '#default_value' => (isset($values['ecc_levels']["level$i"]["cache_value"])) ? $values['ecc_levels']["level$i"]["cache_value"] : '',
        '#prefix' => '<div class="cache-value">',
        '#suffix' => '</div>',
      );
    }

    return $items;
  }

  /**
   * Context execute.
   *
   * @param Array $result
   *   Delivery callback result
   */
  public function execute($result) {
    $contexts = context_active_contexts();
    foreach ($contexts as $context) {
      if (!empty($context->reactions['ecc']) && !user_access('dont generate http headers')) {
        if (isset($context->conditions['node'])) {
          _context_ecc_set_node_cache($result, $context);
        }
        else {
          _context_ecc_set_cache($context);
        }
      }
    }
  }

}
