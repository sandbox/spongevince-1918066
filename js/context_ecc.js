(function ($) {
    Drupal.behaviors.ContextEcc = {
        attach: function (context, settings) {
            $("input[name*='[second_since_last_change_second]']").each(function (index) {
                $(this).change(function () {
                    var next_line = index + 2;
                    $("input[name='reactions[plugins][ecc][ecc_levels][level" + next_line + "][second_since_last_change_first]']").val($(this).val());
                });
            });
        }
    };
})(jQuery);
